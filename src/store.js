import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)
import apiUrl from '../utils/consts'

export default new Vuex.Store({
  state: {
    classes: [],
    studentsInClass: {}, // Dictionary of classes represented by their ID. Each class is array of its students.
    studentsDictionary: {}, // Dictionary of all studentsrepresented by their ID. Each student stores its info.
    selectedClass: '',
    fetchingData: false
  },
  getters: {

  },
  mutations: {
    SET_classes (state, payload) {
      // console.log('payload: ', payload)
      state.classes = payload
    },
    SET_studentsDictionary (state, payload) {
      let studentId
      let studentInfo = {}
      for (studentId in payload) {
        studentInfo = payload[studentId]
        state.studentsDictionary[studentId] = studentInfo
      }
    },
    SET_studentsInClass (state, payload) {
      let selectedClass = state.selectedClass
      state.studentsInClass[selectedClass] = payload
    },
    SET_selectedClass (state, payload) {
      state.selectedClass = payload
    },
    UPDATE_studentsDictionary (state, payload) {
      let studentId = payload.id
      state.studentsDictionary[studentId] = payload
    },
    RESET_studentsDictionary (state) {
      state.studentsDictionary = {}
    },
    RESET_studentsInClass (state) {
      state.studentsInClass = {}
    },
    SET_fetchingData (state) {
      state.fetchingData = !state.fetchingData
    },
    RESET_fetchingData (state) {
      state.fetchingData = false
    }
  },
  actions: {
    getClassesData ({ commit }) {
      /* Get classes data from server */
      let path = 'classes'

      axios.get(apiUrl + path).then((response)=>{
        // console.log('response: ', response)
        commit('SET_classes', response.data)
      })
    },
    getStudentsListByClassId ({ commit }, id) {
      /* Get students of class from server */
      let path = 'classes/' + id + '/students'
      commit('SET_fetchingData')
      // Promised used to insure that all data has arrived and stored properly in vuex states. Only then data manipulation can be done (used in StudentsList.vue)
      return new Promise((resolve) => {
        resolve(
          axios.get(apiUrl + path).then((response)=>{
            let studentsArr = response.data
            // Storing students list in a state
            commit('SET_studentsInClass', studentsArr)
          
            // Fetching students info by student id
            let studentObj = {}
            let finalObj = {}
            let studentId

            // Get student in parallel
            return new Promise((resolve) => {
              let requests = studentsArr.map((student) => {
                // console.log('student: ', student)
                return new Promise((resolve) => {
                  axios.get(apiUrl + 'students/' + student.id).then((response) => {
                    // console.log('student.id: ', student.id, 'response: ', response)
                    studentObj[student.id] = response.data
                    resolve(studentObj)
                    studentObj = {}
                  })
                })
              })
    
              // After all students arrived --> create final object that contain all student info data
              resolve(Promise.all(requests).then((values) => {
                for (let i = 0; i < values.length; i++) {
                  studentId = Object.keys(values[i])
                  finalObj[studentId] = values[i][studentId]
                }
                commit('SET_studentsDictionary', finalObj)
                commit('SET_fetchingData')
              })
              )
            })
          })
        )
      })
    },
  },
  plugins: [createPersistedState()]
})
