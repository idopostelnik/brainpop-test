import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Plugins
import BootstrapVue from 'bootstrap-vue' 
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// style scss file
import '../scss/style.scss' 

// Use plugins
Vue.use(BootstrapVue)

Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
